// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular-cli.json`.

export const environment = {
  production: false,
  url:'http://localhost/angular/slim/',
  firebase: {
    apiKey: "AIzaSyAIzStQ9W77urFYTqlxSBNRxVI1vdush4A",
    authDomain: "messages-12afe.firebaseapp.com",
    databaseURL: "https://messages-12afe.firebaseio.com",
    projectId: "messages-12afe",
    storageBucket: "messages-12afe.appspot.com",
    messagingSenderId: "630499981209"
  }
};
